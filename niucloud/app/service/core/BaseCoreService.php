<?php
// +----------------------------------------------------------------------
// | Niucloud-admin 企业快速开发的saas管理平台
// +----------------------------------------------------------------------
// | 官方网址：https://www.niucloud-admin.com
// +----------------------------------------------------------------------
// | niucloud团队 版权所有 开源版本可自由商用
// +----------------------------------------------------------------------
// | Author: Niucloud Team
// +----------------------------------------------------------------------

namespace app\service\core;

use app\service\BaseService;

/**
 * 系统基础服务层
 * Class BaseCoreService
 * @package app\service\core
 */
class BaseCoreService extends BaseService
{


    public function __construct()
    {
        parent::__construct();
    }
}