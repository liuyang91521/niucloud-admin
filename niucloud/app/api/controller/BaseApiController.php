<?php
// +----------------------------------------------------------------------
// | Niucloud-admin 企业快速开发的saas管理平台
// +----------------------------------------------------------------------
// | 官方网址：https://www.niucloud-admin.com
// +----------------------------------------------------------------------
// | niucloud团队 版权所有 开源版本可自由商用
// +----------------------------------------------------------------------
// | Author: Niucloud Team
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace app\api\controller;

use app\BaseController;
use think\App;

/**
 * api基础控制器
 * Class BaseApiController
 * @package app\api\controller
 */
class BaseApiController extends BaseController
{

    public function initialize()
    {

    }

}